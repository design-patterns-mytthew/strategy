package eu.mytthew;

public class Circle {
	ColorChanger colorChanger;
	private final int radius;
	private int color = 0;

	public Circle(int radius) {
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public void setColorChanger(ColorChanger colorChanger) {
		this.colorChanger = colorChanger;
	}

	public void update() {
		colorChanger.updateColor(this);
	}

	@Override
	public String toString() {
		return "Circle[r=" + radius + ", c=" + color + "]";
	}
}
