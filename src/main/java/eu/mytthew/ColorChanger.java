package eu.mytthew;

public interface ColorChanger {
	void updateColor(Circle circle);
}
