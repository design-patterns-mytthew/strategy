package eu.mytthew;

public class Strategy {
	public static void main(String[] args) {
		ColorChanger colorChanger = circle -> circle.setColor(1);
		Circle c1 = new Circle(12);
		c1.setColorChanger(colorChanger);
		c1.update();
		System.out.println(c1);
	}
}
